**Trendyol Database Bootcamp - PostgreSQL Ödevi**

Maddelerin üzerine tıklayarak ilgili sayfalara ulaşabilirsiniz.

1. [Fiziksel replikasyon kurunuz. Kurulum komut setlerini ve pg_replication_slot çıktılarını paylaşınız.](https://gitlab.com/acciorg/database/-/blob/master/2Mayis/firstPart.md)

2. [pg_dump ile trendyol veritabanının dump'ını alınız. Aldığınız dump'ı yeni bir veritabanı create ederek içine restore ediniz. Komut setlerini paylaşınız.](https://gitlab.com/acciorg/database/-/blob/master/2Mayis/secondPart.md)

3. [Aynı sunucu üzerine veya farklı bir backup sunucusuna pgbackrest kurunuz ve örnek bir backup alınız.](https://gitlab.com/acciorg/database/-/blob/master/2Mayis/thirdPart.md)
