**Part3#**

pgBackRest kurulumunu aşağıdaki gibi tamamladım, öncelikle makineye root kullanıcısıyla login oldum:

`# yum install -y pgbackrest
`
```
# yum install https://download.postgresql.org/pub/repos/yum/13/redhat/rhel-8-x86_64/postgresql13-13.2-1PGDG.rhel8.x86_64.rpm
Last metadata expiration check: 0:32:15 ago on Fri 07 May 2021 04:43:44 PM +03.
postgresql13-13.2-1PGDG.rhel8.x86_64.rpm                                                                                     878 kB/s | 1.4 MB     00:01
Package postgresql13-13.2-1PGDG.rhel8.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```


Daha sonra postgres kullanıcısına geçerek pgbackrest'i aşağıdaki gibi kontrol edebiliriz.

```
# su - postgres
$ pgbackrest
pgBackRest 2.33 - General help

Usage:
    pgbackrest [options] [command]

Commands:
    archive-get     Get a WAL segment from the archive.
    archive-push    Push a WAL segment to the archive.
    backup          Backup a database cluster.
    check           Check the configuration.
    expire          Expire backups that exceed retention.
    help            Get help.
    info            Retrieve information about backups.
    repo-get        Get a file from a repository.
    repo-ls         List files in a repository.
    restore         Restore a database cluster.
    stanza-create   Create the required stanza data.
    stanza-delete   Delete a stanza.
    stanza-upgrade  Upgrade a stanza.
    start           Allow pgBackRest processes to run.
    stop            Stop pgBackRest processes from running.
    version         Get version.
```
pgbackrest'in izinlerini aşağıdaki gibi değiştirdim:

```
chmod 750 /var/lib/pgbackrest
chown postgres: /var/lib/pgbackrest
```
**/etc/pgbackrest.conf** dosyası, çıktısı aşağıdaki gibi olacak şekilde düzenlenir:

```
# cat /etc/pgbackrest.conf
[global]
repo1-path=/var/lib/pgbackrest
repo1-retention-full=2

[global:archive-push]
compress-level=3

[pg01-backup]
pg1-path=/pg_data/13/main
```


Daha sonra **$PGDATA/postgresql.conf** dosyasını aşağıdaki satırları ekleyecek şekilde düzenledim.

```
archive_mode = on
archive_command = 'pgbackrest --stanza=pg01 archive-push %p'
max_wal_senders = 3
```
Ardından PostgreSQL servisini yeniden başlattım.

`# systemctl restart postgresql-13.service`

Yeniden postgres kullanıcısına geçerek son yapılan değişiklikler aşağıdaki gibi kontrol edilebilir:

```
# su - postgres
$ psql
# select name,setting from pg_settings where name like 'archive%' ;
          name           |                     setting
-------------------------+-------------------------------------------------
 archive_cleanup_command |
 archive_command         | pgbackrest --stanza=pg01-backup archive-push %p
 archive_mode            | on
 archive_timeout         | 0
(4 rows)
```

```
# show wal_level;
 wal_level
-----------
 replica
(1 row)
```
postgres kullanıcısındayken stanza oluşturulur:

```
$ pgbackrest --stanza=pg01-backup --log-level-console=info stanza-create
2021-05-07 15:36:12.976 P00   INFO: stanza-create command begin 2.33: --exec-id=1949-bcda482b --log-level-console=info --pg1-path=/pg_data/13/main --repo1-path=/var/lib/pgbackrest --stanza=pg01-backup
2021-05-07 15:36:13.595 P00   INFO: stanza-create for stanza 'pg01-backup' on repo1
2021-05-07 15:36:13.605 P00   INFO: stanza-create command end: completed successfully (630ms)
```
Yukarıdaki işlemin başarıyla sonuçlanıp sonuçlanmadığı aşağıdaki komutlarla kontrol edilebilir:

```
$ pgbackrest --stanza=pg01-backup --log-level-console=info check
2021-05-07 15:36:23.036 P00   INFO: check command begin 2.33: --exec-id=1951-205afb0e --log-level-console=info --pg1-path=/pg_data/13/main --repo1-path=/var/lib/pgbackrest --stanza=pg01-backup
2021-05-07 15:36:23.654 P00   INFO: check repo1 configuration (primary)
2021-05-07 15:36:23.862 P00   INFO: check repo1 archive for WAL (primary)
2021-05-07 15:36:24.065 P00   INFO: WAL segment 000000010000000000000001 successfully archived to '/var/lib/pgbackrest/archive/pg01-backup/13-1/0000000100000000/000000010000000000000001-57707e571035bffe4e0e2b2ddac688503d48cecb.gz' on repo1
2021-05-07 15:36:24.065 P00   INFO: check command end: completed successfully (1030ms)
```
```
$ pgbackrest --stanza=pg01-backup --log-level-console=info backup
2021-05-07 15:37:50.681 P00   INFO: backup command begin 2.33: --exec-id=1956-438a6979 --log-level-console=info --pg1-path=/pg_data/13/main --repo1-path=/var/lib/pgbackrest --repo1-retention-full=2 --stanza=pg01-backup
WARN: no prior backup exists, incr backup has been changed to full
2021-05-07 15:37:51.404 P00   INFO: execute non-exclusive pg_start_backup(): backup begins after the next regular checkpoint completes
2021-05-07 15:37:51.912 P00   INFO: backup start archive = 000000010000000000000003, lsn = 0/3000028
2021-05-07 15:37:52.275 P01   INFO: backup file /pg_data/13/main/base/16385/1255 (648KB, 1%) checksum 0788cbab600cd8def36bd1c7ec679ddea864897b
2021-05-07 15:37:52.294 P01   INFO: backup file /pg_data/13/main/base/14384/1255 (648KB, 3%) checksum 0788cbab600cd8def36bd1c7ec679ddea864897b
2021-05-07 15:37:52.315 P01   INFO: backup file /pg_data/13/main/base/14383/1255 (648KB, 5%) checksum 0788cbab600cd8def36bd1c7ec679ddea864897b
2021-05-07 15:37:52.336 P01   INFO: backup file /pg_data/13/main/base/1/1255 (648KB, 7%) checksum 0788cbab600cd8def36bd1c7ec679ddea864897b

...

2021-05-07 15:37:55.527 P01   INFO: backup file /pg_data/13/main/base/1/1417 (0B, 100%)
2021-05-07 15:37:55.529 P00   INFO: full backup size = 32MB
2021-05-07 15:37:55.529 P00   INFO: execute non-exclusive pg_stop_backup() and wait for all WAL segments to archive
2021-05-07 15:37:55.740 P00   INFO: backup stop archive = 000000010000000000000003, lsn = 0/3000138
2021-05-07 15:37:55.742 P00   INFO: check archive for segment(s) 000000010000000000000003:000000010000000000000003
2021-05-07 15:37:55.762 P00   INFO: new backup label = 20210507-183751F
2021-05-07 15:37:55.790 P00   INFO: backup command end: completed successfully (5111ms)
2021-05-07 15:37:55.790 P00   INFO: expire command begin 2.33: --exec-id=1956-438a6979 --log-level-console=info --repo1-path=/var/lib/pgbackrest --repo1-retention-full=2 --stanza=pg01-backup
2021-05-07 15:37:55.793 P00   INFO: expire command end: completed successfully (3ms)
```

pgbackrest ile full backup almış olduk. Aldığımız backup'ı restore etmek için öncelikle root kullanıcısına geçtim.


```
# rm -rf /pg_data/13/main/*
# pgbackrest --stanza=pg01-backup restore
# pgbackrest info
stanza: pg01-backup
    status: ok
    cipher: none

    db (current)
        wal archive min/max (13): 000000010000000000000001/000000010000000000000003

        full backup: 20210507-183751F
            timestamp start/stop: 2021-05-07 15:37:51 / 2021-05-07 15:37:55
            wal start/stop: 000000010000000000000003 / 000000010000000000000003
            database size: 32MB, database backup size: 32MB
            repo1: backup set size: 3.9MB, backup size: 3.9MB
```


```
# systemctl restart postgresql-13.service
# systemctl status postgresql-13.service
● postgresql-13.service - PostgreSQL 13 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-13.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/postgresql-13.service.d
           └─13-main.conf
   Active: active (running) since Fri 2021-05-07 19:09:28 +03; 1s ago
     Docs: https://www.postgresql.org/docs/13/static/
  Process: 1884 ExecStartPre=/usr/pgsql-13/bin/postgresql-13-check-db-dir ${PGDATA} (code=exited, status=0/SUCCESS)
 Main PID: 1889 (postmaster)
    Tasks: 9 (limit: 5795)
   Memory: 63.0M
   CGroup: /system.slice/postgresql-13.service
           ├─1889 /usr/pgsql-13/bin/postmaster -D /pg_data/13/main/
           ├─1891 postgres: logger
           ├─1897 postgres: checkpointer
           ├─1898 postgres: background writer
           ├─1900 postgres: stats collector
           ├─1905 postgres: walwriter
           ├─1906 postgres: autovacuum launcher
           ├─1907 postgres: archiver last was 00000003.history
           └─1908 postgres: logical replication launcher

May 07 19:09:28 reyhan.test systemd[1]: Starting PostgreSQL 13 database server...
May 07 19:09:28 reyhan.test postmaster[1889]: 2021-05-07 19:09:28.156 +03 [1889] LOG:  redirecting log output to logging collector process
May 07 19:09:28 reyhan.test postmaster[1889]: 2021-05-07 19:09:28.156 +03 [1889] HINT:  Future log output will appear in directory "log".
May 07 19:09:28 reyhan.test systemd[1]: Started PostgreSQL 13 database server.
```

Böylece backup'ı restore etmiş olduk.

**KAYNAKLAR:**
1. https://access.crunchydata.com/documentation/pgbackrest/2.01/user-guide/#:~:text=pgBackRest%20is%20always%20able%20to,since%20the%20last%20full%20backup.
2. https://medium.com/trendyol-tech/dba-g%C3%BCnl%C3%BCkleri-10-veritaban%C4%B1-yedekleme-mimarileri-ve-%C3%A7%C3%B6z%C3%BCmleri-a0eca40c83cd






