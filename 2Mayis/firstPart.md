**PART1#** 

Ödev için öncelikle biri "main" biri "replica" olmak üzere iki tane CentOS 8 sanal makine oluşturdum. Makinelerimizi güncel hale getirip eğer herhangi bir kernel paketi yüklendiyse yendiden başlatıyoruz. İlk olarak dil paketlerini indirdim.

`# dnf install -y glibc-all-langpacks`

PostgreSQL, CentOS 8’in varsayılan AppStream reposunda mevcut ve kurabileceğimiz birden çok sürüm var. Burada RPM paketleri ve bağımlılıkları "module stream" ismiyle tutuluyor, dolayısıyla PostgreSQL'in hangi versiyonunu indirmek istiyorsak öncelikle ilgili modülü etkinleştirmemiz gerekli.

Öncelikle AppStream'de mevcut PostgreSQL modüllerini listeliyoruz:

```
# dnf module list postgresql
CentOS Stream 8 - AppStream                     9.7 kB/s | 4.4 kB     00:00
CentOS Stream 8 - AppStream                     1.6 MB/s | 7.9 MB     00:04
CentOS Stream 8 - BaseOS                         13 kB/s | 3.9 kB     00:00
CentOS Stream 8 - BaseOS                        2.1 MB/s | 2.6 MB     00:01
CentOS Stream 8 - Extras                        2.5 kB/s | 1.5 kB     00:00
CentOS Stream 8 - AppStream
Name         Stream   Profiles             Summary
postgresql   9.6      client, server [d]   PostgreSQL server and client module
postgresql   10 [d]   client, server [d]   PostgreSQL server and client module
postgresql   12       client, server [d]   PostgreSQL server and client module
postgresql   13       client, server [d]   PostgreSQL server and client module

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
```

Eğer herhangi bir işlem yapmadan PostgreSQL'i indirmek istersem, default modül postgresql:10 olduğu için bu versiyonu indirecek. Örneğin son versiyon için deneyecek olursak:

```
# dnf module enable postgresql:13
# dnf install -y postgresql postgresql-server postgresql-contrib
```
Bu şekilde PostgreSQL'i indirebiliriz.

```
# psql --version
psql (PostgreSQL) 13.2
```
Eğitimde kurulumu doğrudan PostgreSQL'in reposunu kullanarak yaptığımız için ben de kurulumu yukarıdaki yöntemle değil, aşağıdaki gibi yaptım. Öncelikle gerekli repoyu sistemimize indiriyoruz; daha sonra daha önce listelediğimiz, AppStream'de bulunan PostgreSQL modüllerini disable ediyoruz. Son adımda da kurulumu gerçekleştiriyoruz.

```
dnf install -y https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm
dnf -qy module disable postgresql
dnf install -y postgresql13 postgresql13-server postgresql13-contrib
```

Red Hat ailesi dağıtımlarında, /var/lib/pgsql/data/ PostgreSQL veritabanı varsayılan konumudur. Fakat bunu değiştirebiliyoruz. Öncelikle PostgreSQL veritabanı dosyalarını tutmak için yeni bir dizin oluşturuyoruz, ardından oluşturduğumuz dizinin owner'ını ve grubunu postgres olarak değiştirdikten sonra bu dizinde sadece postgres kullanıcısına tam yetki verecek şekilde (rwx) dosya izinlerini yeniden ayarlıyoruz. 

```
# mkdir -p /pg_data/13/main
# chown -R postgres: /pg_data
# chmod 700 -R /pg_data
```
Servis konfigürasyonunu tamamlayabilmek için öncelikle /etc/systemd/system/ altında postgresql-13.service.d isminde bir dizin oluşturuyoruz. Dizini oluşturduktan sonra bu dosya yolunda 13-main.conf altında bir dosya yaratıyoruz.

```
# mkdir /etc/systemd/system/postgresql-13.service.d
# vim /etc/systemd/system/postgresql-13.service.d/13-main.conf
```

Yeni oluşturmuş olduğumuz dosyayı aşağıdaki gibi düzenliyoruz:

```
# cat /etc/systemd/system/postgresql-13.service.d/13-main.conf
[Service]
Environment=PGDATA=/pg_data/13/main/
```

Servislerle ilgili herhangi bir değişiklik yaptığımızda systemd'nin bu değişiklikleri algılayıp uygulayabilmesi için unit dosyaları reload etmemiz gerekiyor. Servisi çalştırmadan önce PostgreSQL veritabanını başlatıyoruz, -k parametresiyle data page checksum özelliğini etkinleştirmiş oluyoruz. Her reboot sonrası PostgreSQL servisinin çalışıyor olmasını istediğimiz için servisi enable ederek başlatıyoruz.

```
# systemctl daemon-reload
# su - postgres -c "initdb -D /pg_data/13/main -k"
# su - postgres -c "pg_ctl -D /pg_data/13/main -l logfile start"
# systemctl enable --now postgresql-13
```
Servisin durumunu aşağıdaki gibi görüntüleyebiliriz:

```
# systemctl status postgresql-13.service
● postgresql-13.service - PostgreSQL 13 database server
   Loaded: loaded (/usr/lib/systemd/system/postgresql-13.service; enabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/postgresql-13.service.d
           └─13-main.conf
   Active: active (running) since Wed 2021-05-05 23:50:05 +03; 7min ago
     Docs: https://www.postgresql.org/docs/13/static/
  Process: 29187 ExecStartPre=/usr/pgsql-13/bin/postgresql-13-check-db-dir ${PGDATA} (code=exited, status=0/SUCCESS)
 Main PID: 29193 (postmaster)
    Tasks: 8 (limit: 5795)
   Memory: 16.9M
   CGroup: /system.slice/postgresql-13.service
           ├─29193 /usr/pgsql-13/bin/postmaster -D /pg_data/13/main/
           ├─29194 postgres: logger
           ├─29196 postgres: checkpointer
           ├─29197 postgres: background writer
           ├─29198 postgres: walwriter
           ├─29199 postgres: autovacuum launcher
           ├─29200 postgres: stats collector
           └─29201 postgres: logical replication launcher

May 05 23:50:05 reyhan.test systemd[1]: Starting PostgreSQL 13 database server...
May 05 23:50:05 reyhan.test postmaster[29193]: 2021-05-05 23:50:05.234 +03 [29193] LOG:  redirecting log output to logging collector process
May 05 23:50:05 reyhan.test postmaster[29193]: 2021-05-05 23:50:05.234 +03 [29193] HINT:  Future log output will appear in directory "log".
May 05 23:50:05 reyhan.test systemd[1]: Started PostgreSQL 13 database server.
```
Daha sonra gerekli firewalld yapılandırmalarını tamamlıyoruz, PostgreSQL'in default portu 5432'yi kullanacağımız için firewalld tarafında portla ilgili bir değişiklik yapmıyoruz. Değişiklikleri uygulayabilmesi için Firewalld'yi reload ediyoruz. Firewalld tarafından izin verilen servisleri listeleyip postgresql'in bu servisler arasında olduğunda emin oluyoruz.

```
# firewall-cmd --add-service=postgresql --permanent
# systemctl reload firewalld.service
# firewall-cmd --list-services
cockpit dhcpv6-client postgresql ssh
```

Daha sonra herhangi bir soruna sebep olmasını istemediğim için SELinux'u da hem bu oturum hem de reboot sonrası disabled bir hale getirdim:

```
# setenforce 0
# sed -i '/SELINUX=enforcing/c\SELINUX=disabled' /etc/selinux/config
```
Bu adımdan sonra postgres kullanıcısına geçip environment değişikliklerini tamamlıyoruz:

```
# su - postgres
$ vim /var/lib/pgsql/.pgsql_profile
   PGDATA=/pg_data/13/main
   export PGDATA
   PATH=$PATH:/usr/pgsql-13/bin
   export PATH
```

Hala postgres kullanıcısındayken psql komutunu çalıştırıp replication için kullanıcı oluşturdum. Var olan kullanıcıları "\du" ile listeleyebiliriz.

```
$ psql
# CREATE USER rep_user REPLICATION LOGIN ENCRYPTED PASSWORD 'rep_pass';
# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 rep_user  | Replication                                                | {}
```

Daha sonra "trendyol" isminde bir database oluşturdum.

```
# CREATE DATABASE trendyol ENCODING='UTF-8' LOCALE = 'tr_TR.UTF-8' TEMPLATE template0;
CREATE DATABASE
# \l
                                  List of databases
   Name    |  Owner   | Encoding |   Collate   |    Ctype    |   Access privileges
-----------+----------+----------+-------------+-------------+-----------------------
 postgres  | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 |
 template0 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 template1 | postgres | UTF8     | en_US.UTF-8 | en_US.UTF-8 | =c/postgres          +
           |          |          |             |             | postgres=CTc/postgres
 trendyol  | postgres | UTF8     | tr_TR.UTF-8 | tr_TR.UTF-8 |
(4 rows)
```

Daha sonra PostgreSQL servisinin tüm interfacelerini dinlemesi için aşağıdaki işlemler yapılır.


```
# alter system set listen_addresses TO '0.0.0.0';
# select pg_reload_conf();
```

```
# select name from pg_settings where pending_restart ;
       name
------------------
 listen_addresses
```


Yukarıdaki çıktıda listen_addresses ile ilgili yaptığımızın değişikliğin uygulanabilmesi için servisi restart etmemiz gerektiğini görüyoruz. Bu yüzden tekrar **root** kullanıcısına geçerek aşağıdaki komutu çalıştırıyoruz:

`# systemctl restart postgresql-13`

PostgreSQL servisini restart ettikten sonra **postgres** kullanıcısına geçiyoruz. Az önce yaptığımız değişikliği kontrol ediyoruz.


```
# su - postgres
$ psql
```

```
# show listen_addresses;
 listen_addresses
------------------
 0.0.0.0
(1 row)
```

PostgreSQL'de login olabilen roller kullanıcıları, login olamayan roller grupları oluşturur. Sonraki adım için psql'deyken **"dba" **isminde bir grup yaratıp, önceki adımlarda oluşturduğumuz kullanıcıyı bu gruba ekleyeceğiz.

```
# create role dba;
# grant dba to rep_user;
# \du
                                   List of roles
 Role name |                         Attributes                         | Member of
-----------+------------------------------------------------------------+-----------
 dba       | Cannot login                                               | {}
 postgres  | Superuser, Create role, Create DB, Replication, Bypass RLS | {}
 rep_user  | Replication                                                | {dba}

```
Erişimlerle ilgili adımları tamamlamaya devam ediyoruz. Son yarattığımız dba grubunun trendyol database'ine erişebilmesi için gerekli konfigürasyon değişikliklerini yaptım. Aşağıdaki dosyayı düzenlemek için postgres kullanıcısı olmalıyız, çıktısı aşağıdaki gibi olacak şekilde düzenliyoruz.

```
$  grep -v '^#' $PGDATA/pg_hba.conf
local   all             all                                     peer
host    all             all             127.0.0.1/32            scram-sha-256
host    trendyol        +dba            ip-subnet               scram-sha-256
```

Değişikliklerin uygulanmasını sağlamak için aşağıdaki işlemler yapılır:


```
$ psql
# select pg_reload_conf();
```
```
postgres=# select * from pg_hba_file_rules;
 line_number | type  |  database  | user_name |    address    |     netmask     |  auth_method  | options | error
-------------+-------+------------+-----------+---------------+-----------------+---------------+---------+-------
          84 | local | {all}      | {all}     |               |                 | peer          |         |
          86 | host  | {all}      | {all}     | 127.0.0.1     | 255.255.255.255 | scram-sha-256 |         |
          94 | host  | {trendyol} | {+dba}    | <ip-address>  | <netmask>       | scram-sha-256 |         |
(3 rows)
```

Güvenliği artırmak için ssl'i etkinleştiriyoruz, yeniden **postgres** kullanıcısına geçilerek işlemler aşağıdaki gibi yapılabilir:

```
$ cd $PGDATA
$ openssl req -new -x509 -days 1095 -nodes -text \
>     -out server.crt -keyout server.key \
>     -subj "/CN=db.trendyol.com"
$ chmod 600 server.key server.crt
```

```
$ psql
# alter system set ssl to on;
```

```
# show ssl_key_file;
 ssl_key_file
--------------
 server.key
(1 row)
```

```
# show ssl_cert_file;
 ssl_cert_file
---------------
 server.crt
(1 row)
```

```
# select pg_reload_conf();
# show ssl;
 ssl
-----
 on
```
Buraya kadarki işlemleri iki sunucu için de uygulamıştım. Aşağıdaki adımları sadece main makinesinde uyguladım.

**[Main]**

Öncelikle pg_hba.conf dosyasına gerekli satırı aşağıdaki çıktıyı vermesini sağlayacak şekilde, postgres kullanıcısındayken, ekliyorum.

```
$ grep -v '^#' $PGDATA/pg_hba.conf
local   all             all                                     peer
host    all             all             127.0.0.1/32            scram-sha-256
host    trendyol        +dba            192.168.113.1/24        scram-sha-256
host    replication     rep_user        192.168.113.1/24        scram-sha-256
$pgsql
```
Daha sonra "standboy_slot1" adında bir slot oluşturdum, true parametresiyle oluşturduğumuz slot'un ihtiyaç duyacağı en eski WAL'i tutabilmesini sağlıyoruz.

```
# SELECT pg_create_physical_replication_slot('standby_slot1',true);
 pg_create_physical_replication_slot
-------------------------------------
 (standby_slot1,0/1894918)
(1 row)
```
Oluşturduğumuz slot'lara aşağıdak gibi bakabiliriz:

```
# select * from pg_replication_slots;
   slot_name   | plugin | slot_type | datoid | database | temporary | active | active_pid | xmin | catalog_xmin | restart_lsn | confirmed_flush_lsn | wal_sta
tus | safe_wal_size
---------------+--------+-----------+--------+----------+-----------+--------+------------+------+--------------+-------------+---------------------+--------
----+---------------
 standby_slot1 |        | physical  |        |          | f         | f      |            |      |              | 0/1894918   |                     | reserve
d   |
(1 row)
```
Daha sonra "replica" makinesinde, postgres kullanıcıyla base backup işlemini yaptım, burada main makinesinde oluşturduğum slot'ı da parametre olarak belirttim.

**[Replica]**

```
# su - postgres
$ rm -rf /pg_data/13/main/*
```

**$ pg_basebackup --pgdata=/pg_data/13/main --write-recovery-conf --checkpoint=fast --slot=standby_slot1 --wal-method=stream --verbose --progress --dbname="host=192.168.113.136 user=rep_user sslmode=disable"**

Yukarıdaki komutu çalıştırdıktan sonra rep_user kullanıcısının parolasını giriyorum. Komutun çıkardığı verbose aşağıdaki gibi:

```
pg_basebackup: initiating base backup, waiting for checkpoint to complete
pg_basebackup: checkpoint completed
pg_basebackup: write-ahead log start point: 0/4000028 on timeline 1
pg_basebackup: starting background WAL receiver
33467/33467 kB (100%), 1/1 tablespace
pg_basebackup: write-ahead log end point: 0/4000100
pg_basebackup: waiting for background process to finish streaming ...
pg_basebackup: syncing data to disk ...
pg_basebackup: renaming backup_manifest.tmp to backup_manifest
pg_basebackup: base backup completed
```

Bu şekilde backup'ı almış olduk. 


**KAYNAKLAR**

1. Eğitim Notları
2. https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/selinux_users_and_administrators_guide/sect-managing_confined_services-postgresql-configuration_examples 
3. https://www.postgresql.org/docs/13/functions-admin.html






