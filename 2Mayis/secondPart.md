**PART2#**

Trendyol database'inin dump'ını alıp "dump" isimli bir veritabanına restore edeceğim için öncelikle yeni database'i oluşturdum. 

```
$ psql
# CREATE database dump ENCODING='UTF-8' LC_COLLATE = 'tr_TR.UTF-8' LC_CTYPE='tr_TR.UTF-8' TEMPLATE template0 owner rep_user;
```
Sonrasında tekrar **postgres** kullanıcısına geçerek **pg_hba.conf** dosyasında **"dump"** veritabanı için **"dba"** grubuyla ilgili aşağıdak satırı ekledim, dosyanın hali aşağıdaki gibi olmalı:

```
# grep -v '^#' $PGDATA/pg_hba.conf
local   all             all                                     peer
host    all             all             127.0.0.1/32            scram-sha-256
host    trendyol        +dba            192.168.113.1/24        scram-sha-256
host    dump            +dba            192.168.113.1/24        scram-sha-256
host    replication     rep_user        192.168.113.1/24        scram-sha-256
```
Trendyol veritabanının dump'ını aşağıdaki gibi aldım:

`$ pg_dump -h localhost -U rep_user -W trendyol -F t > first_dump.tar`

Son oluşturduğum veritabanına restore ettim:

`$ pg_restore -h localhost -U rep_user -d dump first_dump.tar`

**KAYNAKLAR**

1. https://www.postgresql.org/docs/13/app-pgdump.html
2. https://www.postgresql.org/docs/13/backup-dump.html
